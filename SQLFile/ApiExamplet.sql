/****** Object:  Database [ApiExample]    Script Date: 13/03/2022 02:24:28 p. m. ******/
CREATE DATABASE [ApiExample]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ApiExample', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\ApiExample.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ApiExample_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\ApiExample_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [ApiExample] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ApiExample].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ApiExample] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ApiExample] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ApiExample] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ApiExample] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ApiExample] SET ARITHABORT OFF 
GO
ALTER DATABASE [ApiExample] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ApiExample] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ApiExample] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ApiExample] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ApiExample] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ApiExample] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ApiExample] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ApiExample] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ApiExample] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ApiExample] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ApiExample] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ApiExample] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ApiExample] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ApiExample] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ApiExample] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ApiExample] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ApiExample] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ApiExample] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ApiExample] SET  MULTI_USER 
GO
ALTER DATABASE [ApiExample] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ApiExample] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ApiExample] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ApiExample] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ApiExample] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ApiExample] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [ApiExample] SET QUERY_STORE = OFF
GO
USE [ApiExample]
GO
/****** Object:  Table [dbo].[Department]    Script Date: 13/03/2022 02:24:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Department](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Location] [varchar](50) NULL,
 CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 13/03/2022 02:24:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Salary] [float] NULL,
	[DeptoId] [int] NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Department] ON 

INSERT [dbo].[Department] ([Id], [Name], [Location]) VALUES (1, N'Sales', N'USA')
INSERT [dbo].[Department] ([Id], [Name], [Location]) VALUES (2, N'RRHH', N'USA')
INSERT [dbo].[Department] ([Id], [Name], [Location]) VALUES (3, N'RRHH', N'Chile')
INSERT [dbo].[Department] ([Id], [Name], [Location]) VALUES (4, N'RRHH', N'Colombia')
INSERT [dbo].[Department] ([Id], [Name], [Location]) VALUES (5, N'IT', N'Colombia')
INSERT [dbo].[Department] ([Id], [Name], [Location]) VALUES (6, N'IT', N'Peru')
INSERT [dbo].[Department] ([Id], [Name], [Location]) VALUES (7, N'Legal Department', N'Peru')
INSERT [dbo].[Department] ([Id], [Name], [Location]) VALUES (8, N'Legal Department', N'China')
INSERT [dbo].[Department] ([Id], [Name], [Location]) VALUES (9, N'Techincal', N'Sindey')
SET IDENTITY_INSERT [dbo].[Department] OFF
GO
SET IDENTITY_INSERT [dbo].[Employee] ON 

INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (1, N'Juan', 212760, 1)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (2, N'Pedro', 126294, 2)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (3, N'Camilo', 569332, 3)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (4, N'Arturo', 101797, 4)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (5, N'Jean', 231422, 5)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (6, N'Paul', 28941, 6)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (7, N'Alex', 511649, 7)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (8, N'Daniel', 210012, 8)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (9, N'Camila', 259338, 1)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (10, N'Laura', 695440, 2)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (11, N'Paola', 406724, 3)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (12, N'Deisy', 169746, 4)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (13, N'Catalina', 871159, 5)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (14, N'Carolina', 835435, 6)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (15, N'Fernanda', 579111, 7)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (16, N'Julian', 708165, 8)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (17, N'Mathew Howard', 996854, 1)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (18, N'Abigail Clarke', 473773, 2)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (19, N'Colleen Bonilla', 305348, 3)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (20, N'Rebecca White', 450069, 1)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (21, N'Alicia Hanson', 850381, 2)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (22, N'Jenna Hamilton', 616609, 3)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (23, N'Courtney Adkins', 901407, 4)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (24, N'Nathan Taylor', 696640, 5)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (25, N'Christopher Brown', 393656, 6)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (26, N'Timothy Crawford MD', 765393, 7)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (27, N'Natalie Lewis', 354270, 6)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (28, N'Cory Jacobs', 373600, 7)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (29, N'Bryan Warner', 140369, 8)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (30, N'Bryan Guzman', 885724, 2)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (31, N'Charles Miller', 120966, 3)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (32, N'Victoria Manning', 174283, 4)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (33, N'Dylan Johnson', 285963, 5)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (34, N'Stephanie Johnson', 127254, 7)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (35, N'Russell Smith', 79125, 7)
INSERT [dbo].[Employee] ([Id], [Name], [Salary], [DeptoId]) VALUES (36, N'Sandra Rivera', 265259, 8)
SET IDENTITY_INSERT [dbo].[Employee] OFF
GO
/****** Object:  Index [IX_Employee]    Script Date: 13/03/2022 02:24:28 p. m. ******/
CREATE NONCLUSTERED INDEX [IX_Employee] ON [dbo].[Employee]
(
	[DeptoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Department] FOREIGN KEY([DeptoId])
REFERENCES [dbo].[Department] ([Id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Department]
GO
USE [master]
GO
ALTER DATABASE [ApiExample] SET  READ_WRITE 
GO
