﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace CermTest.Models
{
    public class Department
    {

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
    }

    public class ResposeEmployeCount
    {
        public int Quantiy { get; set; }
        public string DepartmentName { get; set; }
        public string Location { get; set; }
    }

    public class Response
    {
        public int code { get; set; }
        public string Description { get; set; }

        public Object Data {get;set;}
    }



    public enum ErrorResponsesCodes
    {
        Credentials,
        NoAvailable

    }


    public static class ErrorResponsesHandle
    {
        public static Response GetErrorResponses(this ErrorResponsesCodes excep)
        {
            Response e = new Response();
            switch (excep)
            {
                case ErrorResponsesCodes.Credentials:
                    e.Description = "SQL Credentials Fails"; e.code = 406;
                    return e;
                case ErrorResponsesCodes.NoAvailable:
                    e.Description = "SQL no available"; e.code = 407;
                    return e;
                default:
                    e.Description = "Undefined Error"; e.code = 412;
                    return e;
            }
        }


    }
}
