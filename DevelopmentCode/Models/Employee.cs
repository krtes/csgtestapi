﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CermTest.Models
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public float Salary { get; set; }
        public int DeptoId { get; set; }
    }

  
}
