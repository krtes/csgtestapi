﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CermTest.Data;
using CermTest.Models;
using Microsoft.Data.SqlClient;

namespace CermTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        private readonly CermDBContext _context;

        public DepartmentsController(CermDBContext context)
        {
            _context = context;
        }


        [HttpGet]
        [Route("/{url}/GetEmployeeByDepartment")]

        public Response GetEmployeeByDepartment()
        {
            Response r = new Response();

            r.code = 200;
            r.Description = "OK";

            List<ResposeEmployeCount> Result = new List<ResposeEmployeCount>();

            try
            {
                Result = (from d in _context.Department
                          join e in _context.Employee on d.Id equals e.DeptoId into gj
                          from result in gj.DefaultIfEmpty()

                          group result by new { d.Location, d.Name, d.Id } into DepartmentGrouped

                          select new ResposeEmployeCount
                          {
                              DepartmentName = DepartmentGrouped.Key.Name,
                              Location = DepartmentGrouped.Key.Location,
                              Quantiy = DepartmentGrouped.Count(e => e.Id != null),
                          }).OrderByDescending(rs => rs.Quantiy).ToList<ResposeEmployeCount>();
                r.Data = Result;
            }
            catch (SqlException e)
            {
                if (e.Class == 20)
                {

                    r = ErrorResponsesHandle.GetErrorResponses(ErrorResponsesCodes.NoAvailable);
                }
            }
            catch (InvalidOperationException)
            {

                r = ErrorResponsesHandle.GetErrorResponses(ErrorResponsesCodes.Credentials);

            }


            return r;
        }
    }
}
