﻿using CermTest.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CermTest.Data
{
    public class CermDBContext : DbContext
    {
        public CermDBContext(DbContextOptions<CermDBContext> options) : base(options)
        {


        }
        public DbSet<Department> Department { get; set; }
        public DbSet<Employee> Employee { get; set; }



    }
}
